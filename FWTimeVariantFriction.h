﻿/*
 *  Copyright (c) 2003-2012, Shoichi Hasegawa and Springhead development team 
 *  All rights reserved.
 *  This software is free software. You can freely use, distribute and modify this 
 *  software. Please deal with this software under one of the following licenses: 
 *  This license itself, Boost Software License, The MIT License, The BSD License.   
 */

#ifndef FWHAPTIC_SAMPLE_H
#define FWHAPTIC_SAMPLE_H

#include <Springhead.h>
#include <Framework/SprFWApp.h>
#include <Framework/SprFWConsoleDebugMonitor.h>
#include <../../dependency/src/al/al.h>
#include <../../dependency/src/al/alc.h>


using namespace Spr;

class FWTimeVariantFriction;
class MyConsoleDebugMonitor : public FWConsoleDebugMonitor {
	virtual void ExecCommand(std::string cmd, std::string arg, std::vector<std::string> args);
	virtual void Candidates(std::vector<std::string>& rv, size_t fieldStart, std::string field);
	virtual bool ProcessKey(int key);
	FWTimeVariantFriction* app;
public:
	MyConsoleDebugMonitor(FWTimeVariantFriction* a):app(a) {}
};

struct LogFrame {
	double time;
	double mu;
	Vec3f frictionForce;
	Vec3f totalForce;
	Vec3f verticalForce;
	Vec3f proxyAccel;
	unsigned contactCount;
	unsigned frictionCount;
	PHSolidPairForHapticIf::FrictionState frictionState;
};
std::ostream& operator << (std::ostream&os, const LogFrame& l);

class FWTimeVariantFriction : public FWApp{
public:
	MyConsoleDebugMonitor console;
	PHSceneIf* phscene;			// PHSceneへのポインタ

	PHHapticPointerIf* pointer; // 力覚ポインタへのポインタ
	PHSolidIf* floor;			// 床
	int floorId;
	float pdt;					// 物理スレッドの刻み
	float hdt;					// 力覚スレッドの刻み
	int physicsTimerID;			// 物理スレッドのタイマ
	int hapticTimerID;			// 力覚スレッドのタイマ
	UTRef<HIBaseIf> device;		// 力覚インタフェースへのポインタ
	HIHapticDummyIf* dummyDevice;	// キーボードで操作するダミーデバイス
	bool bPause;				//	シミュレーションの一時停止
	bool bDrawGraph;			//	グラフ描画
	RingBuffer<LogFrame> logRing;	//	HapticRenderingの結果
	std::vector<LogFrame> logGraph;	//	グラフ表示
	int hapticCount;
	std::ofstream log;			//	ログファイル

	FWTimeVariantFriction();
	void Display();
	void InitInterface();						// 力覚インタフェースの初期化
	void Init(int argc = 0, char* argv[] = 0);	// アプリケーションの初期化
	virtual void BuildScene();						// オブジェクトの作成
	virtual void Keyboard(int key, int x, int y);	// キーボード関数
	void TimerFunc(int id);							// 各タイマが呼ぶコールバック関数
	void UpdateMaterial(const PHMaterial& m);
		void IdleFunc();
	void Cleanup();

	//	マイク入力の表示（加速度表示関係）
	ALCdevice* acInputDevice;
	ALenum acErrorCode;
	//	static const size_t acSampleFreq = 22050;	// Sample rate
	static const size_t acSampleFreq = 1000;	// Sample rate
	std::vector<short> acBuffer;				// captured data
	UTCriticalSection acLock;

	bool bAudioCapture;

	void AudioCaptureInit();
	void AudioCaptureStart();
	void AudioCaptureLoop();
	void AudioCaptureStop();
	void AudioCaptureCleanup();
};

#endif
