﻿/*
 *  Copyright (c) 2003-2012, Shoichi Hasegawa and Springhead development team 
 *  All rights reserved.
 *  This software is free software. You can freely use, distribute and modify this 
 *  software. Please deal with this software under one of the fqowing licenses: 
 *  This license itself, Boost Software License, The MIT License, The BSD License.   
 */

#include "FWTimeVariantFriction.h"	
#include <conio.h>
#include <time.h>
#pragma comment(lib,"OpenAL32.lib")
#pragma comment(lib,"alut.lib")

using namespace Spr;


bool MyConsoleDebugMonitor::ProcessKey(int key) {
	if (key == 0x1B) {
		app->Keyboard(key, 0, 0);
		return true;
	}
	else {
		return FWConsoleDebugMonitor::ProcessKey(key);
	}
}

void MyConsoleDebugMonitor::Candidates(std::vector<std::string>& rv, size_t fieldStart, std::string field){
	UTTypeDescIf* td = UTTypeDescIf::FindTypeDesc("PHMaterial", "Physics");
	CandidatesForDesc(rv, td, field);
}
void MyConsoleDebugMonitor::ExecCommand(std::string cmd, std::string arg, std::vector<std::string> args) {
	UTTypeDescIf* td = UTTypeDescIf::FindTypeDesc("PHMaterial", "Physics");	//	CollisionもPhysicsに含まれる
	PHMaterial m = app->pointer->GetShape(0)->GetMaterial();				//	ポインタのMaterialを取得
	if (ExecCommandForDesc(td, &m, cmd, arg, args) == FWConsoleDebugMonitor::WRITE) {
		//	摩擦係数を更新
		app->UpdateMaterial(m);
	}
}



std::ostream& operator << (std::ostream&os, const LogFrame& hr) {
	os << hr.time << "\t" << hr.mu << "\t" << hr.frictionForce.norm() << "\t" << hr.totalForce.y
		<< "\t" << hr.contactCount << "\t" << hr.frictionCount << "\t" << hr.frictionState << "\t" << hr.proxyAccel.x << "\t" << hr.proxyAccel.y
		<< std::endl;
	return os;
}


FWTimeVariantFriction::FWTimeVariantFriction():logRing(100), console(this){
//	_crtBreakAlloc = 82325;
	pdt = 0.02f;
	hdt = 0.001f;
	bPause = false;
	bDrawGraph = true;
	hapticCount=0;
	time_t now = time(NULL);
	struct tm pnow;
	localtime_s(&pnow, &now);
	std::ostringstream os;
	os << "HapticLog_" << 1900 + pnow.tm_year << "_" 
		<< std::setfill('0') << std::setw(2) << pnow.tm_mon
		<< std::setfill('0') << std::setw(2) << pnow.tm_mday << "_"
		<< std::setfill('0') << std::setw(2) << pnow.tm_hour
		<< std::setfill('0') << std::setw(2) << pnow.tm_min << ".txt";
	log.open(os.str().c_str(), std::ios::app);
}

void FWTimeVariantFriction::Display() {
	FWWinIf* win = GetCurrentWin();
	FWSceneIf* scene = win->GetScene();
	GRRenderIf* render = win->GetRender();
	HITrackballIf* trackball = win->GetTrackball();
	if (!scene)
		return;

	scene->Sync();
	// GRSceneにカメラフレームが無い場合はトラックボールを直接ビュー変換に反映する
	if (!scene->GetGRScene() || !scene->GetGRScene()->GetCamera() || !scene->GetGRScene()->GetCamera()->GetFrame()) {
		render->SetViewMatrix(trackball->GetAffine().inv());
	}
	render->ClearBuffer();
	render->BeginScene();
	scene->Draw(render, win->GetDebugMode());

	render->PushProjectionMatrix();
	render->SetProjectionMatrix(Affinef::OrthoGL(Vec3f(0, 0, 10), Vec2f(2, 2), -10, 10));
	Affinef af;
	af.Ey() *= -1;
	af.Ex() *= -1;
	render->SetViewMatrix(af);
	render->SetModelMatrix(Affinef());
	render->SetDepthTest(false);
	render->SetLighting(false);
	/*
	render->SetMaterial(GRRenderIf::WHITE);
	std::ostringstream os2;
	os2 << (pointer->IsTimeVaryFriction() ? "A" : "B")
		<< " ('t' to change)   ";
	render->DrawFont(Vec3f(-1, 0.95f, 0), os2.str());
	*/
	if (bDrawGraph) {		//	グラフの描画
		render->SetMaterial(GRRenderIf::WHITE);
		render->DrawLine(Vec3f(-1, 0, 0), Vec3f(1, 0, 0));
		render->SetMaterial(GRRenderIf::GRAY);
		render->DrawLine(Vec3f(-1, 0.5f, 0), Vec3f(1, 0.5f, 0));
		render->DrawLine(Vec3f(-1, 1, 0), Vec3f(1, 1, 0));
		float dx = 0.001f;
		render->SetMaterial(GRRenderIf::CYAN);
		int tStart = 0;
		if ((int)logGraph.size() - 2 / dx > 0) tStart = (int)((int)logGraph.size() - 2 / dx);
		float x = -1 - tStart * dx;
		for (int t = tStart; t < (int)logGraph.size() - 1; ++t) {
			render->DrawLine(Vec3f(x + t*dx, logGraph[t].mu, 0), Vec3f(x + (t + 1)*dx, logGraph[t + 1].mu, 0));
		}
		render->SetMaterial(GRRenderIf::RED);
		for (int t = tStart; t < (int)logGraph.size() - 1; ++t) {
			render->DrawLine(Vec3f(x + t*dx, logGraph[t].totalForce.norm() / 10, 0),
				Vec3f(x + (t + 1)*dx, logGraph[t + 1].totalForce.norm() / 10, 0));
		}
		render->SetMaterial(GRRenderIf::YELLOW);
		for (int t = tStart; t < (int)logGraph.size() - 1; ++t) {
			render->DrawLine(Vec3f(x + t*dx, logGraph[t].frictionForce.norm() / 10, 0),
				Vec3f(x + (t + 1)*dx, logGraph[t + 1].frictionForce.norm() / 10, 0));
		}
		render->SetMaterial(GRRenderIf::WHITE);
		for (int t = tStart; t < (int)logGraph.size() - 1; ++t) {
			const float off = -0.5f;
			const float sca = 0.0005f;
			render->DrawLine(Vec3f(x + t*dx, logGraph[t].proxyAccel.x*sca + off, 0),
				Vec3f(x + (t + 1)*dx, logGraph[t + 1].proxyAccel.x*sca + off, 0));
		}
		//	加速度センサ
		dx /= (acSampleFreq / 1000.0f);
		acLock.Enter();
		tStart = 0;
		if ((int)acBuffer.size() - 2 / dx > 0) tStart = (int)((int)acBuffer.size() - 2 / dx);
		x = -1 - tStart * dx;
		render->SetMaterial(GRRenderIf::MAGENTA);
		const float audioScale = 65536.0f / 2.0f * 3;
		const float audioOffset = -0.8f;
		for (int t = tStart; t < (int)acBuffer.size() - 1; ++t) {
			render->DrawLine(Vec3f(x + t*dx, acBuffer[t] / audioScale + audioOffset, 0),
				Vec3f(x + (t + 1)*dx, acBuffer[t + 1] / audioScale + audioOffset, 0));
		}
		acLock.Leave();
		render->SetMaterial(GRRenderIf::CYAN);
		render->DrawFont(Vec3f(-1, 0.90f, 0), "Friction coefficient");
		render->SetMaterial(GRRenderIf::YELLOW);
		render->DrawFont(Vec3f(-1, 0.85f, 0), "Friction force");
		render->SetMaterial(GRRenderIf::RED);
		render->DrawFont(Vec3f(-1, 0.80f, 0), "Total force");
		render->SetMaterial(GRRenderIf::WHITE);
		render->DrawFont(Vec3f(-1, 0.75f, 0), "Proxy accel");
		render->SetMaterial(GRRenderIf::MAGENTA);
		render->DrawFont(Vec3f(-1, 0.70f, 0), "Accelerometer");
		render->SetMaterial(GRRenderIf::LIGHTGRAY);
		render->DrawFont(Vec3f(0.8f, 0.95f, 0), "1.0/10N");
		render->DrawFont(Vec3f(0.8f, 0.45f, 0), "0.5/ 5N");
		render->DrawFont(Vec3f(0.8f, -0.05f, 0), "0");
		//DSTR << "logGraph.size() " << logGraph.size() << std::endl;
	}
	
	render->SetMaterial(GRRenderIf::WHITE);
	std::ostringstream os;
	os << (pointer->IsTimeVaryFriction() ? "Proposed time-varying friction" : "Conventional constant friction") 
		<< " ('t' to change)   ";
	os << (pointer->GetHapticRenderMode() == PHHapticPointerDesc::CONSTRAINT ? "Conventional massless proxy" : "Proposed dynamic Proxy") 
		<< " ('y' to change)   ";
	render->DrawFont(Vec3f(-1, 0.95f, 0), os.str());
	
	render->PopProjectionMatrix();
	render->EndScene();
	render->SwapBuffers();
}

void FWTimeVariantFriction::BuildScene() {
	PHSdkIf* phSdk = GetSdk()->GetPHSdk();				// シェイプ作成のためにPHSdkへのポインタをとってくる
	phscene = GetSdk()->GetScene()->GetPHScene();		// 剛体作成のためにPHSceneへのポインタをとってくる
	phscene->SetContactTolerance(0.0002);
	phscene->SetGravity(Vec3d(0, -2.0, 0));

	Vec3d pos = Vec3d(0, 0, 0.1);						// カメラ初期位置
	GetCurrentWin()->GetTrackball()->SetPosition(pos);	// カメラ初期位置の設定
	GetSdk()->SetDebugMode(true);						// デバック表示の有効化
	GetSdk()->GetScene()->EnableRenderHaptic(true);		//	力覚デバッグ表示ON
	GetSdk()->GetScene()->EnableRenderContact(false);	//	接触のデバッグ表示ON

	// 床を作成
	CDBoxDesc bd;
	bd.boxsize = Vec3f(5.0f, 1.0f, 5.0f);

	floor = phscene->CreateSolid();
	floor->AddShape(phSdk->CreateShape(bd));
	floor->SetFramePosition(Vec3d(0, -0.015 - bd.boxsize.y / 2, 0.0));
	floor->SetDynamical(false);
	floor->SetName("soFloor");

	// 箱を作成
#if 1
	PHSolidIf* soBox = phscene->CreateSolid();
	bd.boxsize.clear(0.03f);
	bd.material.density = 10000;
	soBox->AddShape(phSdk->CreateShape(bd));
	soBox->CompInertia();
	soBox->SetCenterPosition(Vec3d(-0.03, 0.035, 0.0));
	soBox->SetName("soBox");
#endif
	// 力覚ポインタの作成
	pointer = phscene->CreateHapticPointer();			// 力覚ポインタの作成
	CDSphereDesc cd;									//　半径1cmの球
	cd.radius = 0.01f;
	cd.material = bd.material;
	bd.boxsize = Vec3f(0.02f, 0.02f, 0.02f);			//	１辺2cmの直方体
	CDShapeIf* shape = phSdk->CreateShape(cd);			//	どちらかを作る
	shape->SetDensity(0.01f / shape->CalcVolume());	//	指の重さは大体 6g
	pointer->AddShape(shape);	// シェイプの追加
	pointer->SetShapePose(0, Posed(Vec3d(), Quaterniond::Rot(Rad(10), 'z')));
	pointer->SetDefaultPose(Posed());					//	力覚ポインタ初期姿勢の設定
	pointer->CompInertia();								//	質量と慣性テンソルを密度から計算
	pointer->SetLocalRange(0.02f);						//	局所シミュレーション範囲の設定
	pointer->SetPosScale(1.0f);							//	力覚ポインタの移動スケールの設定
	pointer->SetFrictionSpring(2000.0f);					//	DynamicProxyの際の摩擦計算に使うバネ係数
//	pointer->SetFrictionSpring(500.0f);					//	DynamicProxyの際の摩擦計算に使うバネ係数
	pointer->SetReflexSpring(3000.0f);					//	力覚レンダリング用のバネ
	pointer->SetReflexDamper(0.0f);						//	力覚レンダリング用のダンパ
	pointer->SetRotationReflexSpring(30.0f);			//	力覚レンダリング用の回転バネ
	pointer->SetRotationReflexDamper(0.0f);				//	力覚レンダリング用の回転ダンパ
	pointer->SetName("hpPointer");
	pointer->EnableRotation(false);
	pointer->EnableFriction(true);
	pointer->EnableVibration(true);
	pointer->SetHapticRenderMode(PHHapticPointerDesc::DYNAMIC_PROXY);
	pointer->EnableTimeVaryFriction(true);
	pointer->EnableForce(true);
	FWHapticPointerIf* fwPointer = GetSdk()->GetScene()->CreateHapticPointer();	// HumanInterfaceと接続するためのオブジェクトを作成
	fwPointer->SetHumanInterface(device);		// HumanInterfaceの設定
	fwPointer->SetPHHapticPointer(pointer);		// PHHapticPointerIfの設定

	//	
	PHHapticEngineIf* he = phscene->GetHapticEngine();
	for (int i = 0; i < he->NSolids(); ++i) {
		if (he->GetSolidPair(i, 0)->GetSolid(0) == floor) {
			floorId = i;
			break;
		}
	}
}

void FWTimeVariantFriction::InitInterface(){
	HISdkIf* hiSdk = GetSdk()->GetHISdk();
	//	実デバイスの追加
	//	for SPIDAR
	// x86
	DRUsb20SimpleDesc usbSimpleDesc;
	hiSdk->AddRealDevice(DRUsb20SimpleIf::GetIfInfoStatic(), &usbSimpleDesc);
	DRUsb20Sh4Desc usb20Sh4Desc;
	for(int i=0; i< 10; ++i){
		usb20Sh4Desc.channel = i;
		hiSdk->AddRealDevice(DRUsb20Sh4If::GetIfInfoStatic(), &usb20Sh4Desc);
	}
	// x64
	DRCyUsb20Sh4Desc cyDesc;
	for(int i=0; i<10; ++i){
		cyDesc.channel = i;
		hiSdk->AddRealDevice(DRCyUsb20Sh4If::GetIfInfoStatic(), &cyDesc);
	}
	hiSdk->AddRealDevice(DRKeyMouseWin32If::GetIfInfoStatic());

	//	インタフェースの取得
	device = hiSdk->CreateHumanInterface(HISpidarGIf::GetIfInfoStatic())->Cast();
	if (device->Init(&HISpidarGDesc("SpidarG6X3F"))) {
		device->Calibration();
	}else{	//	XBOX
		device = hiSdk->CreateHumanInterface(HIXbox360ControllerIf::GetIfInfoStatic())->Cast();
		if (!device->Init(NULL)) {
			device = hiSdk->CreateHumanInterface(HINovintFalconIf::GetIfInfoStatic())->Cast();
			if (!device->Init(NULL)) {
				device = hiSdk->CreateHumanInterface(HIHapticDummyIf::GetIfInfoStatic())->Cast();
				device->Init(NULL);
				dummyDevice = device->Cast();
			}
		}
	}
	hiSdk->Print(DSTR);
	hiSdk->Print(std::cout);
}

void FWTimeVariantFriction::AudioCaptureInit() {
	acErrorCode = 0;
	// Request the default capture device with a half-second buffer
	acInputDevice = alcCaptureOpenDevice(NULL, acSampleFreq, AL_FORMAT_MONO16, acSampleFreq / 2);
	if (acInputDevice) acErrorCode = alcGetError(acInputDevice);
	AudioCaptureStart();
}
void FWTimeVariantFriction::AudioCaptureStart() {
	if (acInputDevice) {
		alcCaptureStart(acInputDevice); // Begin capturing
		acErrorCode = alcGetError(acInputDevice);
		bAudioCapture = true;
	}
}
void FWTimeVariantFriction::AudioCaptureStop() {
	// Stop capture
	if (acInputDevice) {
		alcCaptureStop(acInputDevice);
		bAudioCapture = false;
	}
}
void FWTimeVariantFriction::AudioCaptureLoop() {
	if (!acInputDevice) return;
	// Poll for captured audio
	ALCint samplesIn = 0;  // How many samples are captured
	alcGetIntegerv(acInputDevice, ALC_CAPTURE_SAMPLES, 1, &samplesIn);
	if (samplesIn > 0) {
		acLock.Enter();
		// Grab the sound
		acBuffer.resize(acBuffer.size() + samplesIn + 10000);
		short* buf = &*(acBuffer.begin() + acBuffer.size() - (samplesIn + 10000));
		alcCaptureSamples(acInputDevice, buf, samplesIn);
		acBuffer.resize(acBuffer.size() - 10000);
		acLock.Leave();
	}
}
void FWTimeVariantFriction::AudioCaptureCleanup() {
	if (acInputDevice) alcCaptureCloseDevice(acInputDevice);
}

void FWTimeVariantFriction::Init(int argc, char* argv[]){
	FWApp::Init(argc, argv);							// アプリケーションの初期化
	InitInterface();									// インタフェースの初期化
	AudioCaptureInit();									//	加速度計測のためのマイク入力初期化
	BuildScene();										// オブジェクトの作成
	PHHapticEngineIf* he = phscene->GetHapticEngine();	// 力覚エンジンをとってくる
	he->Enable(true);						            // 力覚エンジンの有効化

#if 0
	// シングルスレッドモード
	he->SetHapticStepMode(PHHapticEngineDesc::SINGLE_THREAD);
	phscene->SetTimeStep(hdt);
#elif 1
	// マルチスレッドモード
	he->SetHapticStepMode(PHHapticEngineDesc::MULTI_THREAD);
	phscene->SetTimeStep(pdt);
#else
	// 局所シミュレーションモード
	he->SetHapticStepMode(PHHapticEngineDesc::LOCAL_DYNAMICS);
	phscene->SetTimeStep(pdt);
#endif

	physicsTimerID = GetTimer(0)->GetID();					// 物理スレッドのタイマIDの取得
	GetTimer(0)->SetMode(UTTimerIf::IDLE);					// 物理スレッドのタイマをIDLEモードに設定
#if 1
	UTTimerIf* timer = CreateTimer(UTTimerIf::MULTIMEDIA);	// 力覚スレッド用のマルチメディアタイマを作成
	timer->SetResolution(1);			// 分解能(ms)
	timer->SetInterval(unsigned int(hdt * 1000));		// 刻み(ms)h
	hapticTimerID = timer->GetID();		// 力覚スレッドのタイマIDの取得
	timer->Start();						// タイマスタート
#else
	UTTimerIf* timer = CreateTimer(UTTimerIf::THREAD);	// 力覚スレッド用のマルチメディアタイマを作成
	timer->SetResolution(1);			// 分解能(ms)
	timer->SetInterval(unsigned int(hdt * 1000));		// 刻み(ms)h
	hapticTimerID = timer->GetID();		// 力覚スレッドのタイマIDの取得
	timer->Start();						// タイマスタート
#endif
	std::cout << ">";	//プロンプト
}


void FWTimeVariantFriction::TimerFunc(int id){
	if(phscene->GetHapticEngine()->GetHapticStepMode() == PHHapticEngineDesc::SINGLE_THREAD){
		if(hapticTimerID == id){
			GetSdk()->GetScene()->UpdateHapticPointers();
			phscene->Step();
		}else if(physicsTimerID == id){
			PostRedisplay();
		}
	}else{	//	multi thread
		if(hapticTimerID == id){
			GetSdk()->GetScene()->UpdateHapticPointers();
			if (!bPause) {
				phscene->StepHapticLoop();
				PHHapticEngineIf* he = phscene->GetHapticEngine();
				if (he->NPointersInHaptic() && he->NSolidsInHaptic()){
					PHSolidPairForHapticIf* sp = he->GetSolidPairInHaptic(floorId, 0);
					static Vec3f lastProxyVel;
					if (sp->GetFrictionState() != sp->FREE && sp->GetSolid(0)->NShape() && sp->GetSolid(1)->NShape()) {
						PHShapePairForHapticIf* sh = sp->GetShapePair(0, 0);
						LogFrame lf;
						if (sh->NIrs() - sh->NIrsNormal() > 0) {
							lf.frictionForce = sh->GetIrForce(sh->NIrsNormal());
						}
						else {
							lf.frictionForce = Vec3d();
						}
						lf.totalForce = he->GetPointerInHaptic(0)->GetHapticForce().v();

						lf.time = hapticCount*hdt;
						lf.frictionState = sp->GetFrictionState();
						lf.mu = sh->GetMu();
						lf.contactCount = sp->GetContactCount();
						lf.frictionCount = sp->GetFrictionCount();
						Vec3f proxyVel = he->GetPointerInHaptic(0)->GetProxyVelocity().v();
						lf.proxyAccel = (proxyVel - lastProxyVel) / hdt;
						lastProxyVel = proxyVel;
						logRing.Write(lf);
					}
					else {
						lastProxyVel.clear();
					}
				}
				phscene->StepHapticSync();
				hapticCount++;
			}
		}else{
			PHHapticEngineIf* he = phscene->GetHapticEngine();
			if (!bPause)
				he->StepPhysicsSimulation();
			PostRedisplay();
		}
	}
}

void FWTimeVariantFriction::IdleFunc() {
	console.KeyCheck();
	LogFrame lf;
	while (logRing.Read(lf)) {
		log << lf;
		if (lf.contactCount == 0 && lf.frictionState != PHSolidPairForHapticIf::FREE) {
			logGraph.clear();
		}
		logGraph.push_back(lf);
	}
	AudioCaptureLoop();
}

void FWTimeVariantFriction::Cleanup() {
	AudioCaptureCleanup();
	log.close();
}

void FWTimeVariantFriction::Keyboard(int key, int x, int y){
	// 各スレッドの共有メモリのアクセス違反回避のために全てのタイマをとめる
	for(int i = 0; i < NTimers(); i++)	GetTimer(i)->Stop();
	float dr = 0.001f;
	switch (key) {
	case 'q':
	case 0x1b:
		// アプリケーションの終了
	{
		Cleanup();
		exit(0);
		break;
	}
	case '1':
	{
		// レンダリングモードをPenaltyに
		DSTR << "Penalty mode" << std::endl;
		pointer->SetHapticRenderMode(PHHapticPointerDesc::PENALTY);
		break;
	}
	case '2':
	{
		// レンダリングモードをConstraintに
		DSTR << "Constraint mode" << std::endl;
		pointer->SetHapticRenderMode(PHHapticPointerDesc::CONSTRAINT);
		break;
	}
	case '3':
	{
		// レンダリングモードをDynamics Constraintに
		DSTR << "Dynamics Constraint mode" << std::endl;
		pointer->SetHapticRenderMode(PHHapticPointerDesc::DYNAMIC_PROXY);
		break;
	}
	case 'a':
		if (bAudioCapture) {
			AudioCaptureStop();
		}
		else {
			acLock.Enter();
			acBuffer.clear();
			acLock.Leave();
			AudioCaptureStart();
		}
		break;
	case 'c':
	{
		// インタフェースのキャリブレーション
		device->Calibration();
		DSTR << "CameraPosition" << std::endl;
		DSTR << GetCurrentWin()->GetTrackball()->GetPosition() << std::endl;
	}
	break;
	case 't':	//	time vary friction
		pointer->EnableTimeVaryFriction(!pointer->IsTimeVaryFriction());
		std::cout << "timeVaryFriction:" << pointer->IsTimeVaryFriction() << std::endl;
		break;
	case 'y':
		if (pointer->GetHapticRenderMode() == PHHapticPointerDesc::DYNAMIC_PROXY) {
			pointer->SetHapticRenderMode(PHHapticPointerDesc::CONSTRAINT);
		}
		else {
			pointer->SetHapticRenderMode(PHHapticPointerDesc::DYNAMIC_PROXY);
		}
		break;
	case 'r':{
		static int fricLevel = 0;
		fricLevel++;
		if (fricLevel > 3) fricLevel = 1;
		PHMaterial m = pointer->GetShape(0)->GetMaterial();
		m.timeVaryFrictionA = fricLevel * 0.1f;
		m.mu0 = m.mu + fricLevel * 0.2f;
		UpdateMaterial(m);
		}
		break;
	case 'f':
	{
		// 力覚提示のON/OFF
		if (pointer) {
			static bool bForce = true;
			if (!bForce) {
				bForce = true;
				pointer->EnableForce(bForce);
				DSTR << "Force: ON" << std::endl;
			}
			else {
				bForce = false;
				pointer->EnableForce(bForce);
				DSTR << "Force: OFF" << std::endl;
			}
		}
	}
	break;
	case 'v':
	{
		// 振動提示のON/OFF
		if (pointer) {
			static bool bVibration = false;
			if (!pointer->IsVibration()) {
				pointer->EnableVibration(true);
				DSTR << "Vibration: ON" << std::endl;
			}
			else {
				pointer->EnableVibration(false);
				DSTR << "Vibration: OFF" << std::endl;
			}
		}
	}
	break;
	case 'd':
	{
		// デバック表示のON/OFF
		static bool bDebug = true;
		if (!bDebug) {
			bDebug = true;
			GetSdk()->GetScene()->EnableRenderHaptic(bDebug);
			DSTR << "Debug Mode: ON" << std::endl;
		}
		else {
			bDebug = false;
			GetSdk()->GetScene()->EnableRenderHaptic(bDebug);
			DSTR << "Debug Mode: OFF" << std::endl;
		}
	}
	break;
	case 'g':
		bDrawGraph = !bDrawGraph;
		break;
	case 'b':
	{
		// 新たに剛体を生成する
		GetSdk()->GetScene()->GetPHScene()->GetHapticEngine()->ReleaseState();
		CDBoxDesc bd;
		bd.boxsize.clear(0.03f);
		bd.material.density = 10000;

		PHSolidIf* box = phscene->CreateSolid();
		box->AddShape(GetSdk()->GetPHSdk()->CreateShape(bd));
		box->CompInertia();
		box->SetFramePosition(Vec3d(-0.03, 0.3, 0.0));
		break;
	}
	case 'P':
		bPause = !bPause;
		break;
	case 'p':
	{
		static int count;
		phscene->StepHapticLoop();
		phscene->StepHapticSync();
		if (count % 20 == 0) phscene->GetHapticEngine()->StepPhysicsSimulation();
		count++;
		break;
	}
	case DVKeyCode::LEFT:
		if (dummyDevice) {
			Posed p = dummyDevice->GetPose();
			p.PosX() -= dr;
			dummyDevice->SetPose(p);
		}
		break;
	case DVKeyCode::RIGHT:
		if (dummyDevice) {
			Posed p = dummyDevice->GetPose();
			p.PosX() += dr;
			dummyDevice->SetPose(p);
		}
		break;
	case DVKeyCode::UP:
		if (dummyDevice) {
			Posed p = dummyDevice->GetPose();
			p.PosY() += dr;
			dummyDevice->SetPose(p);
		}
		break;
	case DVKeyCode::DOWN:
		if (dummyDevice) {
			Posed p = dummyDevice->GetPose();
			p.PosY() -= dr;
			dummyDevice->SetPose(p);
		}
		break;
	default:
		break;
	}
	// 全てのタイマを始動
	for(int i = 0; i < NTimers(); i++)	GetTimer(i)->Start();
}

void FWTimeVariantFriction::UpdateMaterial(const PHMaterial& m){
	//	摩擦係数を更新
	pointer->GetShape(0)->SetMaterial(m);
	floor->GetShape(0)->SetMaterial(m);
	//	HapticEnigneの中のShapePairの摩擦係数を更新
	PHHapticEngineIf* he = phscene->GetHapticEngine();
	PHSolidPairForHapticIf* sp = NULL;
	PHShapePairForHapticIf* sh = NULL;
	if (he->NPointers() && he->NSolids()) {
		sp = he->GetSolidPair(floorId, 0);
		if (sp->GetFrictionState() != sp->FREE && sp->GetSolid(0)->NShape() && sp->GetSolid(1)->NShape()) {
			sh = sp->GetShapePair(0, 0);
		}
	}
	if (sh) {
		CDShapeIf* s1 = pointer->GetShape(0);
		CDShapeIf* s2 = sh->GetFrame(1)->GetShape();
		sh->GetFrame(0)->GetShape()->SetMaterial(m);
		sh->GetFrame(1)->GetShape()->SetMaterial(m);
		sh->UpdateCache();
		pointer->GetShape(0)->SetMaterial(m);
		floor->GetShape(0)->SetMaterial(m);
	}
}